```VIP RANKS```
```BRONZE```
PRICE: 500 DP
- V.I.P Bronze Title
- V.I.P Bronze Gear
- V.I.P Bronze Buff -> +2% to all Stats
- Access to V.I.P Mall
- Access to V.I.P Stone
- Custom Commands (.v buff, .v debuff, .v online, .v mall)
- Access to the Transmog Requester
- Access to the Morph NPC

```SILVER```
PRICE: 750 DP
- Access to all VIP Bronze Commands and Items
- V.I.P Silver Title
- V.I.P Silver Gear
- V.I.P Silver Buff -> +7% to all Stats

```GOLD```
PRICE: 1700 DP 
- Access to all VIP Silver Commands and Items
- V.I.P Gold Title
- V.I.P Gold Gear
- V.I.P Gold Buff -> +17% to all Stats

```PLATINUM```
PRICE: 2450 DP
- Access to all VIP Gold Commands and Items
- V.I.P Platinum Title
- V.I.P Platinum Gear
- V.I.P Platinum Buff -> +22% to all Stats
- Unique VIP Spell Option at the Spell Vendor

```DIAMOND```
PRICE: 3500 DP
- Access to all VIP Platinum Commands and Items
- V.I.P Diamond Title
- V.I.P Diamond Gear
- V.I.P Diamond Buff -> +22% to all Stats
- Unique VIP Spell Option at the Spell Vendor
- 10% Berserk Buff

```EXCLUSIVE```
PRICE: 100€ | 13500 DP
- Access to all VIP Diamond Commands and Items
- V.I.P Exclusive Title
- V.I.P Exclusive Gear
- V.I.P Exclusive Buff -> +27% to all Stats
- Unique VIP Spell Option at the Spell Vendor
- 30% Berserk Buff
- V.I.P Exclusive's Blessing -> +10% to all Stats 
- V.I.P Exclusive Haste Buff -> +8% Haste (Passive)
- 1 Custom Mount

```PRESTIGE```
PRICE : 200€ | 27000 DP
- Access to all VIP Exclusive Commands and Items
- V.I.P Prestige Title
- V.I.P Prestige Gear
- V.I.P Prestige Buff -> +30% to all Stats
- Unique VIP Spell Option at the Spell Vendor
- 30% Berserk Buff
- V.I.P Exclusive's Blessing -> +15% to all Stats
- V.I.P Exclusive Haste Buff -> +11% Haste (Passive)
- 3 Custom Mounts
- 7 Custom Items
  - 2 Weapons
  - 5 Main Set Pieces

——————————————————————
```Custom Gear Prices & Information```
If you plan on purchasing Custom Gear, Spells or Stats, please reach out to <@306227629546012674> or open a ticket in-game!

- Off Set Piece / Bag - 22€
- Main Set Piece - 28€
- Custom Weapon - 35€

``Bundle Packs``
- Full Donor Main Set + Weapons - 250€
- Full Custom Set + Weapons + Bags - 350€

- Full Custom Set + Weapons + Bag for 2nd Character - 275€
- Full Custom Set + Weapons + Bag for 3rd+ Character - 200€

```Custom Spells - Prices and Information```
``Haste``
5% Haste - 12.5€
10% Haste - 25€
15% Haste - 37.5€
20% Haste - 50€

``Stats``
5% Stats - 15€
10% Stats - 30€
15% Stats - 45€
20% Stats - 60€
25% Stats -  75€
30% Stats - 90€


``Spell / Attack Power Passives``
No longer being sold - Will be changed to a Berserk (+ DP) of equivalent value
~~12.5M - 25€
25M - 50€
50M - 100€
100M - 200€
200M (MAX) - 400€~~

``Berserk``
10% Berserk - 25€
20% Berserk - 50€
30% Berserk - 75€
40% Berserk - 100€
50% Berserk - 125€
60% Berserk - 150€
70% Berserk - 175€
80% Berserk - 200€
90% Berserk - 225€
100% Berserk - 250€
150% Berserk - 375€
200% Berserk - 500€
300% Berserk - 750€
400% Berserk - 1000€


```Class & Item Transfers```
- 4€ -> Transfer two Materials / Normal Items
- 5€ -> Transfer 1 Donor Item
- 9€ -> Transfer 2 Donor items.
- 10€ -> Transferring Heart of Azeroth Levels
- 15€ -> Full Character Transfer (Equipped Items, Inventory, HoA Levels, Spells etc.)