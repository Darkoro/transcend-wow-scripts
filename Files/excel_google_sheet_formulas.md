### Always show Amount paid and Remaining DP

**DP**
=WENNFEHLER(WENN(ISTLEER($E3); 0; WENN(ISTLEER($D3); $E3; WENN($E3 - $D3 < 0; "Error!"; $E3 - $D3))); "Error!")

**Amount**
=WENNFEHLER(WENN(ODER(ISTLEER($E5); ISTLEER($D5)); 0; WENN($E5 - $D5 < 0; "Error!"; $D5 / 'Calculation Amounts'!$D$3)); "Error!")

### Always show Remaining DP, only show Amount paid on withdrawal

**DP**
=WENNFEHLER(WENN(ISTLEER($E8); 0; WENN(ISTLEER($D8); $E8; $E8 - $D8)); "Error!")

**Amount**
=WENNFEHLER(WENN(ODER(ISTLEER($E13); ISTLEER($D13)); ""; WENN($E13 - $D13 < 0; "Error!"; $D13 / 'Calculation Amounts'!$D$3)); "Error!")

### Only show Amount paid and Remaining DP on withdrawal

**DP**
=WENNFEHLER(WENN(ODER(ISTLEER($D15); ISTLEER($E15)); ""; WENN($E15 - $D15 < 0; "Error!"; $E15 - $D15)); "Error!")

**Amount**
=WENNFEHLER(WENN(ODER(ISTLEER($E15); ISTLEER($D15)); ""; WENN($E15 - $D15 < 0; "Error!"; $D15 / 'Calculation Amounts'!$D$3)); "Error!")