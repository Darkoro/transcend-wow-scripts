```Normal Boss Events (Mini, Easy, Medium, Hard, MT, SWP```
-> May be hosted any time, Trial Gamemasters should always have an assistant.

You can find the different scripts to run at the bottom of the channel.

**Raid Warnings**:
/rw Do not use a Custom Zerk. VIP Zerks are fine. Don't rush ahead, wait for the rest! Do not beg for extra rewards lest you might lose yours.
/rw REMINDER: Do not roll for items you do not need **or** can't use!

**Reward Scale:**
1 - 20 Players: 2 Easy Rewards
21 - 30 Players: 2 Medium Rewards
31 - 40 Players: 2 Hard Rewards

You may reward less or, if you are getting close to the next 'gap', a mix between both.

**Main Teleport**:
.go xyz 2462 1160 25 169 3.2

----------------------------------------------------------

```Panda Run Event```
-> May be hosted any time, very easy to host.

Teleport the players to the Main Teleport location and let them run to the end of the minefield. If you want to morph them into little pandas, you may do so as well - .morph 10990

**Raid Warning**:
/rw No mounts and spells are permitted.

**Reward Scale**:
1 - 20 Players: 1 Easy & 2 HnS Rewards
21 - 30 Players: 1 Medium & 2 HnS Rewards
31 - 40 Players: 1 Hard & 2 HnS Rewards

You may reward less or, if you are getting close to the next 'gap', a mix between both.

**Main Teleport**:
.go xyz 4968 -4811 0 530

----------------------------------------------------------

```Pick a Door | Window Event```
-> May be hosted any time, pretty easy to host.

We have 10 Doors / Windows. You give the players time to pick a door or window after summoning them. You start at any one Door or Window and type '/roll {Nr. of open Doors}'.
The Door you are currently standing at counts as Door #1. If you get `X rolls 2 ( 1-10 )`, you open the 2nd Door / Window and kick them (`Knockback 35`).
The next Door / Window will then be the new Door / Window #1, going down until you have either 1 player or only 1 Door / Window left.

**Raid Warnings**:
`Pick a Door`
/rw You'll be given time to choose a Door. Failure to do so, choosing an already open one or choosing after I say 'STOP' will result in an instant loss!

`Pick a Window`
/rw You'll be given time to choose a Window. Failure to do so, choosing an already open one or choosing after I say 'STOP' will result in an instant loss!

**Reward Scale**:
`Pick a Door`
`Winners` 10 Pick a Door Rewards
`Losers` 6 Pick a Door Rewards

`Pick a Window`
`Winners`
10 Pick a Window Rewards

`Losers`
5 Pick a Window Rewards

You may choose to add an extra Easy or Medium Boss Reward if you have an exceedingly high attendance.

**Main Teleport**:
.go xyz 1240 1750 127 1

----------------------------------------------------------

```Chopper Event```
-> May be hosted any time, pretty easy to host.

Everyone must be in their own chopper. Passing the Finish Line requires them to state their finished lap in /ra ('/ra 1' or '/ra 2')

**Raid Warnings**:
/rw This Event has 3 winners, requiring you to finish two laps. Once you finish your first lap, say '1' in /ra. Second round done, '2' in /ra.

**Reward Scale**:
`Winners`
10 Chopper Rewards

`Losers`
5 Chopper Rewards

**Main teleport**:
.go xyz -6235 -3903 -60 1

----------------------------------------------------------

```Bounty Hunter Event```
-> May be hosted at any time. Trial Gamemasters should have an assistant.

Find a zone in which you wish to hide the bosses in - *If you can't use '.npc set model', don't go with a city*. Ask a Senior GM+ to change their skin if you want.

**Raid Warnings**:
/rw There's X bosses hidden around the zone Y. Once you find a Boss, you must say 'Found' in /ra. Do not fight the Boss alone!

**Reward Scale**:
1 - 20 Players: 2 Easy Rewards
21 - 30 Players: 2 Medium Rewards
31 - 40 Players: 2 Hard Rewards

You may reward less or, if you are getting close to the next 'gap', a mix between both.

----------------------------------------------------------

```Kamikaze Event```
-> May be hosted at any time, very simple to host.

Players need to jump to their death, trying to land in a circle.

**Raid Warning**:
/rw RULES: No Death Sliding or Mounts. No spells to avoid death or to slow your falling speed. Only jump when I say so!

**Reward Scale**:
`Winners`
10 Kamikaze Event

`Losers`
5 Kamikaze Event

**Main Teleport**:
.go xyz -8707 -3407 344 0

----------------------------------------------------------

```Savory | Noggenfogger Event```
-> Can be hosted whenever & wherever.

A maximum of 3 winners is possible, depending on the raid size.

`Savory`
People need to eat the Savory Deviant Delight and turn into a Pirate to win.

`Noggenfogger`
People need to drink the Noggenfogger Elixir and not turn into a Skeleton to win.

**Raid Warning**:
`Savory`
/rw RULES: You will eat and dispel the Savory Delight only when I say so. You win the round if you're a Pirate, you lose if you're a Ninja. Ninjas go behind me.

`Noggenfogger`
/rw RULES: You will drink and dispel the Noggenfogger only when I say so. You win the round if you're no Skeleton, you lose if you are. Skeletons go behind me.

**Reward Scale**:
*1 - 10 Players:*
`Winners`
1 Medium & 3 HnS Rewards

`Losers`
1 Easy & 1 HnS Rewards

*11 - 40 Players:*
`Winners`
1 Hard & 3 HnS Rewards

`Losers`
1 Medium & 1 HnS Rewards

----------------------------------------------------------

```Rocket Chicken Event```
-> May be hosted at any time.

Choose a zone to place 3 - 5 Rocket Chicken in, make it as easy or as difficult as you like - just make sure you can get there through in-game mechanics. Invite the Chicken Finders and .recall them.

**Reward Scale**:
Finders: 10 Hide and Seek Rewards

----------------------------------------------------------

```Hide and Seek Event```
-> May be hosted at any time.

Choose a zone to hide in and make sure both factions can go to your hiding spot. Hide yourself as easy or as difficult as you like, as long as you can get there through in-game mechanics.
You can drop a hint every 10 minutes if you want.
Invite the Finders and .recall them.

**Reward Scale**:
Finders: 10 Hide and Seek Rewards

----------------------------------------------------------

```Labyrinth Run Event```
-> May be hosted at any time.

First three people to exit the maze win.
The players have to get from one side of the maze to the other.

**Raid Warning**:
/rw Don't run around the maze and don't .ap to people!

**Reward Scale**:
`Winners`
*1 - 20 Players*
2 Easy + 3 HnS Rewards

*21 - 30 Players*
2 Medium + 3 HnS Rewards

*31 - 40 Players*
2 Hard + 3 HnS Rewards

`Losers`
1 Reward (Depending on the Raid size - see above) + 2 HnS Rewards

**Main Teleport**:
.go xyz 3609 366 94.742134 169

----------------------------------------------------------

```Musical Chairs Event```
-> May be hosted at any time. Recommendation is two people to make the chair removal easier.

Set / remove an amount of chairs so that you start with a chair for every person. Take away one chair after every round. People run and stop on your commands, if they cheat by sitting before being told, disqualify them.

**Raid Warning**:
/rw RULES: You run when I say 'RUN' and stop when I stay 'STOP'. Once I say 'STOP', sit on a chair. If there are two people sitting, you'll roll. Higher roll sits. Sitting before being told disqualifies you.

**Reward Scale**:
`Winners`
10 Musical Chairs Reward

`Losers`
5 Musical Chairs Reward

**Main Teleport**:
.go xyz 702 673 96 169

----------------------------------------------------------

```Water Splash Event```
-> May be hosted at any time but requires 2 Gamemasters.

Very similar to the Kamikaze where players jump off of a platform and have to hit a pool of water. The second GM will be down at the pool to check which players actually hit the pool **without dying**.

**Raid Warning**:
/rw RULES: Jump only when I tell you to. No avoiding Death by Invincibility spells or slowing your falling speed. If you hit the pool of water below, you win. If you die, you lose.

**Reward Scale**:
`Winners`
10 Splash Water Event Reward

`Losers`
5 Splash Water Event Reward

**Main Teleports**:
Starting platform: .go xyz -8738 -5051 346 1
Reward Spot: .go xyz -8672 -5073 31 1

----------------------------------------------------------

```Death Dive Event```
-> May be hosted at any time

Very similar to the Chopper Event, it's just underwater. First three to reach the end win.

**Raid Warnings**:
/rw Once you reach the end of the Dive, you need to announce that either by calling your position (e.g. 'Third') or saying 'Done' in '/ra'.
/rw Any form of potion or spell to speed up your underwater travel is prohibited. Water Breathing is allowed! Do not use .ap to other players!

**Rewards**:
`Winners`
10 Water Splash Reward

`Losers`
5 Water Splash Reward

**Main Teleport**:
.go xyz 5260 -5673 1.5 530 6.2

**Reward Platform**:
.go xyz 5699 -5667 1.1 530 4.2

----------------------------------------------------------

```Kill the Gamemaster```
***New Event***
-> May be hosted sporadically. Requires two Gamemasters - one to be fought and one to have the raid group opened.

Every GMs initial host of this requires a Junior Admin+ to add Gear and Spells to the character.
The hosting GM will prepare their character and place themselves in the Gurubashi Arena, the second one will be opening the raid and check for multiboxing and so on.

*Equipment*
Full Donor for the corresponding class

*Possible Spells*
General Class Spells
53736 - Seal of Corruption (If not a Paladin)
6336509 - 90% Damage Reduction
1500822 - 99% Damage Reduction (**Only if big Zerk players join!**)
10056222 - 30% Stats
10056226 - 20% Haste

**Raid Warning**:
/rw Once I say 'START', you engage the GM to kill. You may use anything at your disposal except for 0(G)CD. Good luck!

**Reward Scale**
3 Hard Boss Rewards

----------------------------------------------------------

```Zone Race Event```
***New Event***
-> May be hosted at any time - Up to 5 winners.

Choose any two zones accessible to both factions that are on the same Continent. Gather the players and teleport them to the starting zone. Set everybody's speed to a singular value - make sure it's appropriate for the distance they need to travel!

**Raid Warnings**:
/rw You will all start in this zone and run to ZONE X. The first X people to finish the Race will win.
/rw Your speed will be set to a value appropriate for the distance travelled. You can mount up now or stay dismounted. Do not change your form of travel and don't use the Teleport Pet or teleportation commands!

**Reward Scale**:
`Winners`
*1 - 20 Players*
2 Easy + 3 HnS Rewards

*21 - 30 Players*
2 Medium + 3 HnS Rewards

*31 - 40 Players*
2 Hard + 3 HnS Rewards

`Losers`
1 Reward (Depending on the Raid size - see above) + 2 HnS Rewards

----------------------------------------------------------

```Knowledge Check Event```
***New Event***
-> May be hosted at any time, requires some knowledge of Transcend-WoW History

Get some questions readied up about Transcend-WoW, perhaps also some general WoW Lore Knowledge and gather your party. Teleport them to the Hosting Location, explain the rules and ask away. **Up to 5 winners**

**Raid Warning**:
/rw I will be posting several questions in chat. The first person to answer any one question correctly (Answer via /w) will win the round. A total of X people can win.

**Reward Scale**
`Winners`
1 Medium & 6 HnS Rewards

`Losers`
1 Easy & 3 HnS Rewards

**Main Teleport**:
.go xyz 2462 1160 25 169 3.2

----------------------------------------------------------

```Blackwing Lair Event```
-> Hosted every Friday, 11:00 server time.

The Event is already set up. Remove the Trespass Ender before teleporting the players into Black Wing Lair and set it back once the Event is over.

**Raid Warnings**:
/rw Do not use a Custom Zerk, Custom Spells or 0(G)CD. VIP Zerks are fine. Don't rush ahead, wait for the rest! Do not beg for extra rewards lest you might lose yours.
/rw REMINDER: Do not roll for items you do not need **or** can't use!

**Reward Scale**:
4 Hard Boss Rewards

**Main Teleport**:
.go xyz -7665.66 -1102.2 399.56 469

----------------------------------------------------------

```High Stakes Gate Event```
-> Either this or Death Maze will be hosted at random, Sunday 11:00 server time.

We have the big middle area with the 'base' High Stakes mobs set up as well as two Gates (Blue & Red). One of them will have GM Bosses, the other will be a second wave of normal mobs. After the middle area is cleared, let the players line up and call for a Roll. The two highest rollers will roll again, the winner of which chooses one of the Gates.
The GM Boss Gate will be rolled every week and, if chosen, will be set up manually. The Mob Gate is a Weekly Boss Event, set in the Boss Event zone.

**Raid Warnings**:
/rw We have the big middle area and two Gates for you all. One of the Gates will have GM Bosses behind it, the other will have Mobs from some of our Instances!
/rw After clearing the middle area, you line up and roll upon being told. The two highest rollers will be rolling again.
/rw The winner of that roll will decide which Gate is to be opened - Blue or Red.
/rw Do not use a Custom Zerk, Custom Spells or 0(G)CD. VIP Zerks are fine. Don't rush ahead, wait for the rest! Do not beg for extra rewards lest you might lose yours.
/rw REMINDER: Do not roll for items you do not need **or** can't use!

**Reward Scale:**
1 - 20 Players: 6 Easy Rewards
21 - 30 Players: 6 Medium Rewards
31 - 40 Players: 6 Hard Rewards

You may reward less or, if you are getting close to the next 'gap', a mix between both.

**Main Teleport**:
.go xyz -3808 -2508 132 169 1.6

----------------------------------------------------------

```Weekly Boss Event```
-> This will be hosted Saturday, 11:00 server time every week.

**Raid Warnings**:
/rw Do not use a Custom Zerk, Custom Spells or 0(G)CD. VIP Zerks are fine. Don't rush ahead, wait for the rest! Do not beg for extra rewards lest you might lose yours.
/rw REMINDER: Do not roll for items you do not need **or** can't use!

**Reward Scale**:
4 Hard Boss Rewards

**Main Teleport**:
.go xyz 2462 1160 25 169

----------------------------------------------------------

```Death Maze Event```
-> Either this or High Stakes Gate will be hosted at random, Sunday 11:00 server time.

**Raid Warnings**:
/rw RULES: You won't get a resurrection from any GM unless you fully wipe! You have to resurrect your own party. Top 3 Resurrecters get +1 Reward!
/rw If you manage to clear the whole maze within 30 minutes, you get +1 Reward! If you manage to kill Parasinus without wiping, you get +1 Reward!
/rw If you split up too often and ignore my warnings, you get -1 Reward!
/rw Do not use a Custom Zerk, Custom Spells or 0(G)CD. VIP Zerks are fine. Don't rush ahead, wait for the rest! Do not beg for extra rewards lest you might lose yours.
/rw REMINDER: Do not roll for items you do not need **or** can't use!

**Reward Scale**:
*Base Reward*: 4 Hard Boss Rewards
*Time-based Reward*: +1 Reward (Clear <=30min.)
*Skill-based Reward*: +1 Reward (Kill Parasinus without wipe)
*Amount-based Reward*: +1 Reward (Top 3 Resurrecters)

**Main Teleport**:
.go xyz 1042 72 92 169

----------------------------------------------------------

```NPC IDs for Rewards```
Boss Event Easy - 888880
Boss Event Medium - 888881
Boss Event Hard - 888882
Hide and Seek Reward - 888883
Chopper Reward (Should be spawned) - 888884
Kamikaze Reward - 888899
Musical Chairs Event (Should be spawned) - 888900
Water Splash Event (Should be spawned) - 888901
Pick a Door (Should be spawned) - 100010
Pick a Window (Should be spawned) - 100011
Rocket Chicken - 25109

----------------------------------------------------------------------------------------- 

```High Stakes Gate Boss Monster IDs for Setup```
Boss Event - 189999 - <10>
Boss Event 1 - 888885 - <3>
Boss Event 2 - 888887 <3>
Tempest Revenant - 200004 - <7>
Prince Thunderaan - 200005 - <4>
Diablo - 200007 - <5>
Shark King - 200008 - <5>
Ahune - 253151 - <4>
Water Revenant - 121291 - <7>
Therazane - 253152 - <4>
Earth Revenant - 121292 - <7>
Smolderon - 189930 - <4>
Fire Revenant - 202004 - <7>
Archimonde - 55570 - <1>
Astaroth - 556001 - <1>
Bacchus - 556000 - <1>
Boreas - 566002 - <1>
Notus - 566003 - <1>
GM Boss Vengerful - 1000361 - <1>
GM Boss Ahiru - 1000359 - <1>
GM Boss Pallemaster - 1000366 - <1>
GM Boss Mirel - 1000370 - <1>
GM Boss Luminia - 1000357 - <1>
GM Boss Sarah - 1000362 - <1>
Sarahs Cubs - 2222224 - <2>
GM Boss Carn - 1000358 - <1>
GM Boss Darkside - 1000354 - <1>
GM Boss Fyp - 1000364 - <1>
GM Boss Mami - 1000363 - <1>
Mamis Lions - 2222225 - <2>
GM Boss Jawless - 1000360 - <1>
GM Boss Parasinus - 35329  - <1>