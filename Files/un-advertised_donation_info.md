**Prices we don't advertise**
Prestige VIP + Full Custom Set - 450€
30% Stat Buff for all Characters on the account - 315€
20% Haste Buff for all Characters on the account - 175€
30% Stat + 20% Haste Buffs for all Characters on the account - 470€
*Note: We only do this specific combination for account wide buffs*

**Custom Items**
- Off Set Piece / Bag - 22€
- Main Set Piece - 28€
- Custom Weapon - 35€


**Bundle Packs / Sets**
- Full Donor Main Set + Weapons - 250€
- Full Custom Set + Weapons + Bags - 350€
*The one Character on a Prestige Account with Prestige Gear pays 100€ less*
*The price for any Custom Item is deducted - Example: They already have a weapon (35€). 350€ - 35€ = 315€*

- Full Custom Set + Weapons + Bag for 2nd Character - 275€
- Full Custom Set + Weapons + Bag for 3rd+ Character - 200€

**Haste and Stat Spells**
Please refer to <#876903074348810240>

**Spell / Attack Power Passives**
No longer being sold - Will be changed to a Berserk (+ DP) of equivalent value
~~12.5M - 25€
25M - 50€
50M - 100€
100M - 200€
200M (MAX) - 400€~~

**Berserk**
Please refer to <#876903074348810240>