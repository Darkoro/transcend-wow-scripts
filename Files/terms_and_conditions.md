**Terms and Conditions**
``Members of Staff will follow all rules and regulations and, where applicable, enforce them on the server.``

*Do note that the term 'GM / Gamemaster' is used and meant for all Members of Staff, regardless of their position as a Developer or Gamemaster*

1. Gamemasters do not GIFT items! All items are earned through Events or donated for through our Website for Donor Golden Token.

2. GMs may transmog any way they wish as long as their set is professional.

3. GMs are not to assist players with progressing through Tiers. Neither are they to PvP on their GM Account, unless in the Kill the GM-Event. Any such action is to be done using their Player Account.

4. GMs are required to ask a players permission before summoning them (e.g. /w (name) May I summon you), unless said player is breaking rules.

5. @Trial Game Master may only ban for 1 day or less. This rule does not apply if they have permission from a @Junior Game Master or higher ranked member of Staff.

6. Gamemasters should always respond to tickets in a timely and orderly manner.

8. It is a Gamemasters choice whether to respond to any request for help outside of tickets.

9. @Trial Game Master and @Junior Game Master should be given a small time frame to read and complete tickets before higher ranked Staff do so that they have a chance at growing into their role.

10. GMs should not abuse a player with their spells (e.g. Slap). They are meant as a tool for Events.

11. GM Bosses are used solely in special Events - High Stakes Gate, Omega and Monthly Boss Events. @Senior Admin and higher ranked members of Staff may permit a GM Boss to be spawned outside of these Events.

12. Before any Event starts, the hosting GM is required to give a quick explanation through /rw. Possible explanations are written at each Event in ⁠💫┃event-stuff.

13. Any player is to be given at least a single warning before they receive a harsher punishment in the form of a mute, kick or ban. In extreme cases, the warning may be skipped. Any punishment is to be logged in their corresponding channel!

14. A GM is to ask for permission from @Owner, @Co-Owner, @Staff Manager or @Senior Admin before building a new zone.

15. Any Event zone as well as the teleport spots found in ⁠📡┃teleports may only be frequented by GMs on their GM Accounts or by players when necessary for the completion of a Gamemasters task (e.g. a ticket).

16. A Gamemaster is not to use GM Commands on another Gamemaster unless requested or of a significantly higher station.

17. We do not condone any sort of 'joking' about someone elses family or social structure. This does not mean that, if you are friendly with each other, you may not joke around with yourselves a bit, but do keep in mind that it might insult someone. 'Stop' means 'Stop' and should be taken as such!

18. If you have a problem with any member of Staff that you cannot solve on your own, do reach out to any @Senior Admin, @Staff Manager or @Co-Owner. Worst case scenario cases would be @Owner territory.



We, the Staff Team as a whole, have to act responsible and on a certain level of seriousness, especially on our GM Accounts.
We are the face of this server and we need to respect every player in the correct manner.

All rules should be respected by the GMs. Failure to follow the rules will result in warnings and might result in a demote or removal from the team.
Please make sure to check this channel for updates from time to time. ⁠📑┃terms-and-conditions