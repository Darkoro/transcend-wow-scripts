**Purchasable Spells**
5% Haste - .learn 10056223 - 12.50€
10% Haste - .learn 10056224 - 25€
15% Haste -. learn 10056225 - 37.50€
20% Haste - .learn 10056226 - 50€

5% Stats - .learn 10056217 - 15€
10% Stats - .learn 10056218 - 30€
15% Stats - .learn 10056219 - 45€
20% Stats - .learn 10056220 - 60€
25% Stats - .learn 10056221 - 75€
30% Stats - .learn 10056222 - 90€

10% Berserk - 25 Euros - .learn 11055663
20% Berserk - 50 Euros - .learn 11055664
30% Berserk - 75 Euros - .learn 11055665
40% Berserk - 100 Euros - .learn 11055666
50% Berserk - 125 Euros - .learn 11055667
60% Berserk - 150 Euros - .learn 11055668
70% Berserk - 175 Euros - .learn 11055669
80% Berserk - 200 Euros - .learn 11055670
90% Berserk - 225 Euros - .learn 11055671
100% Berserk - 250 Euros - .learn 11055672
150% Berserk - 375 Euros - .learn 11055700
200% Berserk - 500 Euros - .learn 11055673
300% Berserk - 750 Euros - .learn 11055674
400% Berserk - 1000 Euros - .learn 11055675

Toxic Waste - 250€ - .learn 70274
Judgement of Light - 125€ - Spell being made
Windfury Weapon - 125€ - .learn 58804
Frostbrand Weapon - 125€ - .learn 58796
Seal of Vengeance - 125€ - .learn 31801
Seal of Corruption - 125€ - .learn 53736
Torment Of Bot - 50€ - .learn 10000005
Aspect of the Beast - 25€ - Spell being made
*spells being made will need to be added to New Core. 