﻿F1::
;First load message
Send {ENTER}
sleep, 200
Send, Omega Script is loading.... {ENTER}
sleep, 2000

;teleport at start
Send {ENTER}
sleep, 200
Send, .go xyz 1503.130249 3849.984131 25.704268 169 3.125852 {ENTER}
sleep, 2500

;Boss Event Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 1536.410156 3890.471436 26.727150 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1536.711548 3883.478027 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1537.114014 3874.141602 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1537.478882 3865.994873 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1537.807739 3859.002686 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1538.273193 3849.108643 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1538.575562 3840.924316 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1538.522095 3832.175049 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1538.569458 3824.580078 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1538.593872 3818.175049 25.704117 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1546.358276 3817.843018 25.704060 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1546.995239 3824.251465 25.704060 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1547.834229 3832.363281 25.704060 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1548.635132 3839.880615 25.704060 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1549.562134 3848.581299 25.704060 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1550.674561 3859.022217 25.704060 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1551.334106 3865.990967 25.704060 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1551.142212 3875.892090 25.839874 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1550.628052 3884.626953 27.969074 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1550.039795 3892.725098 27.969074 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200

;Revenants Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 1612.154541 3846.197266 25.704203 169 3.269217 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER} ; earth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1615.837402 3850.608887 25.704409 169 3.080719 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER} ; water
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1611.435669 3854.785156 25.704203 169 3.277071 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER} ; fire
sleep, 200

;Boss Event 1 and Boss Event 2
Send {ENTER}
sleep, 200
Send, .go xyz 1578.424072 3823.913818 28.173618 169 2.481825 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER} ; boss_event_1
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1584.338867 3882.981201 26.510908 169 3.909599 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888887 {ENTER} ; boss_event_2
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1666.133789 3936.243408 26.357431 169 4.491580 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER} ; boss_event_1
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1712.804932 3809.929932 27.127228 169 2.481825 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER} ; boss_event_1
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1750.836548 3898.470459 25.769590 169 3.773729 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888887 {ENTER} ; boss_event_2
sleep, 200

;Boss Event Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 1656.091064 3824.000244 27.368481 169 1.225109 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1660.623413 3825.726807 26.625271 169 2.758995 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1657.847412 3829.920654 25.704056 169 4.321937 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1653.469482 3828.816650 25.879801 169 5.684600 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1708.580200 3890.897949 24.868773 169 0.563802 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1707.644775 3894.636719 25.432161 169 5.519665 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1713.823364 3894.857178 25.017920 169 3.776081 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1714.024170 3891.121582 25.031912 169 2.440904 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1759.078247 3817.199219 25.964666 169 6.100862 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1761.604736 3814.377197 26.513229 169 1.095519 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1765.244507 3816.012451 25.600237 169 2.629404 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1762.948242 3818.872559 25.210714 169 4.451528 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1816.464111 3878.732666 27.081219 169 0.973784 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1819.210327 3879.785400 26.766151 169 2.438552 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1819.008789 3882.849609 26.734432 169 3.675555 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1815.920410 3882.473145 27.179049 169 5.627269 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200

;Revenants Event
Send {ENTER}
sleep, 200
Send, .go xyz 1857.155518 3828.780518 26.288994 169 2.781769 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER} ; earth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1863.830688 3833.528320 25.760302 169 2.789623 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER} ; water
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1859.411133 3840.656494 25.028164 169 2.793546 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER} ; fire
sleep, 200

;Revenants Mobs + Diablo
Send {ENTER}
sleep, 200
Send, .go xyz 1904.013550 3762.828857 27.367910 169 2.478612 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1910.898193 3771.618896 27.440504 169 2.458977 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1907.917236 3767.370361 27.420149 169 2.590915 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER} ; diablo_boss
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1961.936523 3825.541992 27.192789 169 2.796699 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1966.029785 3835.859619 26.623535 169 2.792772 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1964.850464 3829.966309 26.964897 169 3.021313 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER} ; diablo_boss
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1973.463989 3753.665283 28.640724 169 2.608206 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1980.967041 3764.815918 28.063856 169 2.549301 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1978.205322 3758.677490 28.915037 169 2.801403 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER} ; diablo_boss
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2034.335938 3826.942871 24.401787 169 2.335673 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2041.508911 3835.260010 24.764252 169 2.590927 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2038.714233 3830.503174 24.401274 169 2.615263 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER} ; diablo_boss
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2054.298096 3772.630371 24.305069 169 2.693807 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER} ; diablo_boss
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2057.813965 3779.949707 24.305069 169 2.693807 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER} ; diablo_boss
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2100.205078 3774.996094 24.304482 169 2.699310 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2107.698730 3785.094482 24.304482 169 2.286977 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2103.474365 3779.674072 24.305069 169 2.803765 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER} ; diablo_boss
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2089.532227 3747.996826 24.304482 169 2.381224 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2098.331055 3757.354004 24.304482 169 2.396932 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2093.485352 3753.503662 24.305069 169 2.811619 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER} ; diablo_boss
sleep, 200

;Revenants Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 2117.654785 3711.476563 24.051414 169 2.268128 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER} ; earth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2125.934326 3708.892822 23.222069 169 2.251634 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER} ; water
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2124.596924 3717.471680 23.780804 169 2.220218 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER} ; fire
sleep, 200

;GM Boss
Send {ENTER}
sleep, 200
Send, .go xyz 2121.291504 3713.974365 23.780546 169 2.314237 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 100073 {ENTER} ;Friday
sleep, 200

;Boss Event 1 and Boss Event 2
Send {ENTER}
sleep, 200
Send, .go xyz 2095.625244 3661.572266 24.171120 169 1.110694 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER} ; boss_event_1
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2141.487793 3656.745361 25.756744 169 2.328062 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888887 {ENTER} ;boss_event_2
sleep, 200

;Sharks and Water Revenant Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 2100.788330 3632.924805 24.235710 169 1.586239 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER} ; shark
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2148.749756 3633.738525 27.457233 169 2.065332 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER} ; shark
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2146.917969 3611.767578 25.921125 169 2.214255 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2102.530518 3609.733643 24.235847 169 0.994077 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2117.240723 3598.061523 23.221939 169 1.582311 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER} ; shark
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2128.151367 3598.617676 23.628771 169 1.559567 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER} ; shark
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2167.081787 3575.747559 25.413307 169 2.128978 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER} ;shark
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2078.940918 3569.064697 25.159258 169 1.088326 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER} ; shark
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2100.117676 3553.342041 24.297503 169 0.981181 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2147.440430 3559.047363 26.829922 169 1.998272 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2122.706299 3543.770264 23.221447 169 1.583126 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER} ; shark
sleep, 200

;Revenants Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 2118.064697 3481.216064 23.221699 169 1.385587 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER} ; earth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2123.328125 3475.582520 23.221771 169 1.549025 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER} ; water
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2127.414795 3481.631104 23.311884 169 1.581937 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER} ; fire
sleep, 200

;GM Boss
Send {ENTER}
sleep, 200
Send, .go xyz 2123.274658 3481.719727 23.326653 169 1.545882 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 1000255 {ENTER} ; Scrat
sleep, 200

;World Bosses
Send {ENTER}
sleep, 200
Send, .go xyz 2076.709229 3428.067627 25.323771 169 1.002288 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 55570 {ENTER} ; archimonde
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2084.517334 3379.173096 25.752529 169 1.453893 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 556001 {ENTER} ; astaroth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2121.153320 3326.322266 21.027122 169 1.844262 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 556000 {ENTER} ; bacchus
sleep, 200

;Boss Event 1 and Boss Event 2
Send {ENTER}
sleep, 200
Send, .go xyz 2156.913818 3281.016846 19.168781 169 1.724092 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER} ; boss_event_1
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2190.882568 3329.751465 15.285325 169 3.330232 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888887 {ENTER} ; boss_event_2
sleep, 200

;Frenzied Worgen Event
Send {ENTER}
sleep, 200
Send, .go xyz 2204.486816 3244.120850 15.285498 169 1.975226 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200006 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2229.263184 3277.618408 15.285498 169 2.713502 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200006 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2240.270996 3313.723145 15.285498 169 3.09679 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200006 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2258.010742 3286.714111 15.284966 169 5.319635 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200006 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2263.038818 3251.601563 15.285015 169 3.513221 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200006 {ENTER}
sleep, 200

;Terokk Event
Send {ENTER}
sleep, 200
Send, .go xyz 2271.651123 3282.495361 15.527161 169 2.684626 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2253.632080 3246.823730 15.285015 169 0.603320 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2276.170898 3224.402344 15.286084 169 2.58784 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2270.738525 3221.804443 15.286084 169 2.016071 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2233.802246 3208.928467 15.285498 169 2.595692 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2227.056152 3207.011963 15.285498 169 1.994862 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200

;Revenants Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 2262.629883 3135.982666 14.973005 169 1.53148 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER} ; earth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2270.773926 3129.952393 13.764444 169 1.794584 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER} ; water
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2276.790527 3139.801514 14.385333 169 2.214776 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER} ; fire
sleep, 200

;Boss Event 1 and Boss Event 2
Send {ENTER}
sleep, 200
Send, .go xyz 2272.396484 3077.808105 4.959257 169 1.763352 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER} ; boss_event_1
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2307.190186 3124.716064 4.705509 169 3.357711 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888887 {ENTER} ; boss_event_2
sleep, 200

;Fire Revenant Event and Ragnaros Event
Send {ENTER}
sleep, 200
Send, .go xyz 2275.140869 3004.036621 4.686969 169 1.584886 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200005 {ENTER} ; ragnaros
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2286.850830 3005.466309 4.686969 169 1.516874 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2295.979004 3005.790283 4.686969 169 1.615049 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2307.641846 3006.600830 4.686969 169 1.744655 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2317.518311 3011.233643 4.686969 169 1.998790 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200005 {ENTER} ; ragnaros
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2328.210938 3014.667969 4.686969 169 1.807487 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2341.410889 3018.031738 4.686969 169 2.263018 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2349.573242 3024.355957 4.686969 169 2.263018 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2356.065918 3031.457764 4.686969 169  2.266611 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200005 {ENTER} ; ragnaros
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2363.631104 3041.906982 4.686969 169 2.428737 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2371.687256 3052.337402 4.686969 169 2.546544 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2377.306152 3061.270508 4.686969 169 2.546544 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2381.610840 3071.466553 4.686969 169  2.574306 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200005 {ENTER} ; ragnaros
sleep, 200

;Ysondre
Send {ENTER}
sleep, 200
Send, .go xyz 2393.491943 2992.717041 4.686386 169 2.098711 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14887 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2363.359375 2982.643066 4.686627 169 2.001061 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14887 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2340.611572 2972.104492 4.686627 169 1.922522 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14887 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2320.459961 2962.977783 4.688032 169 1.796858 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14887 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2299.230957 2956.778076 5.433239 169 1.858119 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14887 {ENTER}
sleep, 200

;Taerar
Send {ENTER}
sleep, 200
Send, .go xyz 2362.476074 2881.960205 4.686161 169 2.387477 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14890 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2376.003906 2905.106689 4.686454 169 2.12894 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14890 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2401.966797 2922.767090 4.686454 169 2.196624 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14890 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2421.100830 2940.051514 4.686454 169 2.334068 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14890 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2439.335205 2960.291504 4.686254 169 2.326208 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14890 {ENTER}
sleep, 200

;Emeriss
Send {ENTER}
sleep, 200
Send, .go xyz 2408.751465 2827.830811 4.686303 169 2.237468 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14889 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2430.077393 2841.467773 4.686303 169 2.118873 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14889 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2447.458984 2860.072266 4.686303 169 2.202911 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14889 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2468.697510 2878.761230 4.686303 169 2.279880 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14889 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2486.111572 2899.250732 4.686303 169 2.349781 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14889 {ENTER}
sleep, 200

;Lethon
Send {ENTER}
sleep, 200
Send, .go xyz 2535.294678 2857.198975 4.686364 169 2.444290 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14888 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2514.165039 2835.465088 4.686364 169 2.373604 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14888 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2490.960449 2811.648682 4.686364 169 2.314699 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14888 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2468.773438 2791.595215 4.686364 169 2.203565 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14888 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2441.422607 2776.211914 4.686364 169 2.077903 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 14888 {ENTER}
sleep, 200

;Gorefiend
Send {ENTER}
sleep, 200
Send, .go xyz 2488.377197 2748.429688 4.686773 169 2.022167 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 55563 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2514.122803 2759.506836 4.686773 169 2.018240 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 55563 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2543.114502 2775.670410 4.686773 169 2.226342 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 55563 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2540.506836 2740.236816 4.686773 169 1.994678 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 55563 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2511.657471 2730.159424 4.686773 169 2.034733 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 55563 {ENTER}
sleep, 200

;Revenants Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 2514.831055 2623.512451 24.247246 169 1.612024 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER} ; earth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2525.399658 2617.812744 24.333462 169 1.698416 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER} ; water
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2533.985107 2625.437500 24.333462 169 1.598670 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER} ; fire
sleep, 200

;World Bosses
Send {ENTER}
sleep, 200
Send, .go xyz 2530.726074 2547.450439 24.385414 169 1.684272 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 55570 {ENTER} ; archimonde
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2591.439697 2597.272949 24.332623 169 2.200912 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 556001 {ENTER} ; astaroth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2629.719238 2672.895020 25.121822 169 3.298900 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 556000 {ENTER} ; bacchus
sleep, 200

;Boss Event Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 2580.592773 2721.102295 23.221933 169 4.429881 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2586.204590 2757.199707 23.221933 169 4.665500 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2585.218018 2786.205566 23.221933 169 4.740899 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2584.565918 2809.074219 23.221933 169 4.740899 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2588.317139 2833.757568 23.221933 169 4.528837 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2598.588379 2862.605957 23.221933 169 4.357620 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200

;SWP Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 2635.137939 2876.619873 25.306419 169 3.974946 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25599 {ENTER} ; cataclysm_hound
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2618.481201 2895.539063 23.222095 169 4.28095 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25599 {ENTER} ; cataclysm_hound
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2596.416504 2902.055664 23.221838 169 4.692801 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25599 {ENTER} ; cataclysm_hound
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2569.314209 2923.840820 23.222301 169 5.559539 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25595 {ENTER} ; cahos_gazer
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2581.479248 2937.889404 24.878145 169 5.316068 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25595 {ENTER} ; cahos_gazer
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2601.575928 2944.211670 25.957796 169 4.734879 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25595 {ENTER} ; cahos_gazer
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2555.674561 2957.413574 25.935650 169 5.529697 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25508 {ENTER} ; shadowsword_guardian
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2587.611816 2990.053467 25.926928 169 4.889605 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25508 {ENTER} ; shadowsword_guardian
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2625.368896 2973.460938 24.097183 169 4.536174 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25508 {ENTER} ; shadowsword_guardian
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2650.458008 2897.536133 27.880348 169 3.781408 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25367 {ENTER} ; sunblade_arch_mage
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2648.076904 2896.224854 27.060513 169 1.374154 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25367 {ENTER} ; sunblade_arch_mage
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2646.549805 2898.862061 27.188841 169 5.862706 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25367 {ENTER} ; sunblade_arch_mage
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2648.905029 2899.447021 27.934301 169 4.570726 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25367 {ENTER} ; sunblade_arch_mage
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2617.868652 2919.500977 25.368647 169 3.971264 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25363 {ENTER} ; sunblade_cabalist
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2614.858643 2921.160400 25.798084 169 4.293276 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25363 {ENTER} ; sunblade_cabalist
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2611.625732 2921.012207 26.004250 169 4.596438 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25363 {ENTER} ; sunblade_cabalist
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2608.380371 2918.549072 25.713083 169 5.604310 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25363 {ENTER} ; sunblade_cabalist
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2614.248291 2946.440674 26.775879 169 4.487797 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 26101 {ENTER} ; fire_fiend
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2617.430176 2946.110596 26.894205 169 4.388483 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 26101 {ENTER} ; fire_fiend
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2619.778076 2945.251953 26.879953 169 4.432582 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 26101 {ENTER} ; fire_fiend
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2622.334473 2944.372559 26.563366 169 4.320519 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 26101 {ENTER} ; fire_fiend
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2625.281006 2943.336670 26.042530 169 4.438081 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25371 {ENTER} ; sunblade_dawn_priest
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2628.773438 2941.535156 25.088261 169 4.231518 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25371 {ENTER} ; sunblade_dawn_priest
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2632.820801 2939.277588 23.845444 169 4.264274 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25371 {ENTER} ; sunblade_dawn_priest
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2636.515625 2938.160645 23.221600 169 4.382084 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25371 {ENTER} ; sunblade_dawn_priest
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2639.851807 2937.610107 23.221600 169 4.464551 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25370 {ENTER} ; sunblade_dusk_priest
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2644.973389 2936.107178 23.221600 169 4.279986 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25370 {ENTER} ; sunblade_dusk_priest
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2649.267578 2934.213623 23.221600 169 4.279986 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25370 {ENTER} ; sunblade_dusk_priest
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2653.883057 2932.159424 23.700399 169 4.211662 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25370 {ENTER} ; sunblade_dusk_priest
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2657.117432 2930.563721 25.759508 169 4.211662 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25369 {ENTER} ; sunblade_vindicator
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2659.730225 2927.985840 26.770220 169 3.818960 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25369 {ENTER} ; sunblade_vindicator
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2662.507568 2924.229980 27.078499 169 3.806394 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25369 {ENTER} ; sunblade_vindicator
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2664.843994 2920.221924 26.617567 169 3.669737 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25369 {ENTER} ; sunblade_vindicator
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2667.495605 2915.226807 27.221375 169 3.634394 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25369 {ENTER} ; sunblade_vindicator
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2667.584473 2950.296143 23.221903 169 3.867656 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25368 {ENTER} ; sunblade_slayer
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2664.555420 2952.852051 23.221903 169 3.924990 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25368 {ENTER} ; sunblade_slayer
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2661.511963 2956.772949 23.221903 169 3.987822 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25368 {ENTER} ; sunblade_slayer
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2657.363770 2958.678223 23.221903 169 3.975256 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25368 {ENTER} ; sunblade_slayer
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2667.053955 2964.904053 23.221903 169 3.763986 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25867 {ENTER} ; sunblade_dragonhawk
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2672.841309 2958.387695 23.221903 169 3.853984 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25867 {ENTER} ; sunblade_dragonhawk
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2681.793701 2970.457275 23.221903 169 3.553177 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25867 {ENTER} ; sunblade_dragonhawk
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2686.110596 2963.062988 23.221903 169 3.407878 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25867 {ENTER} ; sunblade_dragonhawk
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2694.193115 2976.809326 23.221903 169 3.553180 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25867 {ENTER} ; sunblade_dragonhawk
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2698.739014 2967.817871 23.221903 169 3.469143 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25867 {ENTER} ; sunblade_dragonhawk
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2687.596680 2946.936279 26.910213 169 3.700868 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25507 {ENTER} ; sunblade_protector
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2658.128662 2976.879639 25.918571 169 3.732283 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25507 {ENTER} ; sunblade_protector
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2690.032715 2992.662354 24.696526 169 3.626258 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25507 {ENTER} ; sunblade_protector
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2710.145508 2956.934082 27.893528 169 3.465252 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25507 {ENTER} ; sunblade_protector
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2731.836182 2968.365234 26.863178 169 3.575208 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25507 {ENTER} ; sunblade_protector
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2718.674561 3001.213623 26.354809 169 3.445617 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25507 {ENTER} ; sunblade_protector
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2751.750244 3012.915527 25.760666 169 3.347442 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25507 {ENTER} ; sunblade_protector
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2759.193359 2978.024658 25.787945 169 3.465252 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25507 {ENTER} ; sunblade_protector
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2716.975586 2975.880615 23.221882 169 3.321488 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25372 {ENTER} ; sunblade_scout
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2715.450439 2981.767578 23.221882 169 3.627794 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25372 {ENTER} ; sunblade_scout
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2712.462646 2986.278076 23.221882 169 3.583812 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25372 {ENTER} ; sunblade_scout
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2720.345947 2989.950928 23.221882 169 3.558500 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25372 {ENTER} ; sunblade_scout
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2722.391602 2984.101563 23.221882 169 3.442261 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25372 {ENTER} ; sunblade_scout
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2723.691895 2977.102051 23.221882 169 3.321488 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25372 {ENTER} ; sunblade_scout
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2735.959961 2979.889648 23.229586 169 3.22880 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25486 {ENTER} ; sunblade_vanquisher
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2734.805176 2985.783447 23.222029 169 3.503520 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25486 {ENTER} ; sunblade_vanquisher
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2733.658691 2991.418701 23.222029 169 3.479959 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25486 {ENTER} ; sunblade_vanquisher
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2732.417969 2995.297363 23.223501 169 3.558500 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25486 {ENTER} ; sunblade_vanquisher
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2747.409424 2981.127441 23.225746 169 3.118676 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25483 {ENTER} ; shadowsword_manafiend
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2747.799072 2987.359131 23.221870 169 3.224705 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25483 {ENTER} ; shadowsword_manafiend
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2747.845459 2993.262695 23.221870 169 3.342515 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25483 {ENTER} ; shadowsword_manafiend
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2746.859863 2997.477783 23.221870 169 3.330734 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25483 {ENTER} ; shadowsword_manafiend
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2762.918457 2994.561279 23.879110 169 3.504074{ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25506 {ENTER} ; sunblade_lifeshaper
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2760.919678 2999.533203 23.221729 169 3.555125 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25506 {ENTER} ; sunblade_lifeshaper
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2759.268555 3003.393311 23.221729 169 3.520019 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25506 {ENTER} ; sunblade_lifeshaper
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2779.930908 3000.326904 23.222033 169 3.529996 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25484 {ENTER} ; shadowsword_assassin
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2777.536865 3005.284912 23.222033 169 3.492297 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25484 {ENTER} ; shadowsword_assassin
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2775.393311 3010.074707 23.222033 169 3.586545 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25484 {ENTER} ; shadowsword_assassin
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2773.553711 3014.234375 23.708492 169 3.621887 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25484 {ENTER} ; shadowsword_assassin
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2782.646729 3016.366455 23.222263 169 3.535492 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25484 {ENTER} ; shadowsword_assassin
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2785.096191 3013.543701 23.222263 169 3.598325 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25484 {ENTER} ; shadowsword_assassin
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2787.095215 3009.852783 23.222263 169 3.600682 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25484 {ENTER} ; shadowsword_assassin
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2789.214844 3004.125732 23.232750 169 3.529996 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 25484 {ENTER} ; shadowsword_assassin
sleep, 200

;Revenants Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 2817.125488 3021.043213 23.221912 169 3.633619 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER} ; earth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2821.886719 3027.630615 23.221912 169 3.617934 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER} ; water
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2813.116943 3027.078369 23.221912 169 3.727867 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER} ; fire
sleep, 200

;GM Boss
;What boss???

;Earth Revenant Event and Therazane Event
Send {ENTER}
sleep, 200
Send, .go xyz 2841.950928 2988.049072 29.046167 169 2.541960 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2842.471436 3015.865234 25.718330 169 3.764040 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2842.790283 3002.601807 26.902767 169 2.695541 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253152 {ENTER} ; therazane
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2872.400879 3079.671875 26.291365 169 4.204643 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2882.293213 3062.092041 24.556746 169 3.013983 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2879.563232 3070.036621 26.013565 169 3.639589 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253152 {ENTER} ; therazane
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2906.039551 3010.974609 28.045921 169 2.390378 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2906.673828 3031.331299 24.384308 169 3.831584 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2908.932373 3021.928955 25.928457 169 2.979855 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253152 {ENTER} ; therazane
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2943.803711 3049.383301 24.452969 169 2.787006 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2942.982178 3073.488525 26.235098 169 3.697281 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 2943.206055 3060.188232 23.222181 169 3.388263 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253152 {ENTER} ; therazane
sleep, 200

;Revenants Mobs
Send {ENTER}
sleep, 200
Send, .go xyz 3005.330566 3044.070068 23.221804 169 2.644064 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER} ; earth
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3014.230469 3047.848389 23.221804 169 2.946442 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER} ; water
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3006.779785 3053.889893 23.221804 169 3.292018 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER} ; fire
sleep, 200

;Water Revenant Mobs and Ahune Event
Send {ENTER}
sleep, 200
Send, .go xyz 3063.078369 2998.586914 26.275089 169 2.178341 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3072.488770 3025.268311 24.728022 169 3.442833 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3069.358643 3012.066895 24.884165 169 3.035184 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253151 {ENTER} ; ahune
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3113.763916 3033.703125 25.261755 169 2.664503 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3116.425293 3056.431396 23.221918 169 3.673740 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3115.840332 3044.716797 24.372877 169 3.294368 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253151 {ENTER} ; ahune
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3096.373047 3075.339111 26.161747 169 2.816196 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3088.630371 3097.266602 27.022030 169 3.890480 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3093.508789 3087.531494 27.358377 169 3.310076 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253151 {ENTER} ; ahune
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3042.788330 3132.885254 29.463472 169 2.870248 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3031.548828 3154.914063 31.268215 169 4.174009 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 3039.099121 3145.252441 30.306786 169 3.710627 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253151 {ENTER} ; ahune
sleep, 200


$F2::pause
return
$F3::reload