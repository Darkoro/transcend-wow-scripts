F1::
WinActivate ahk_exe Wow.exe

;Boss Event Mob x6
Send {ENTER}
sleep, 200
Send, .go xyz 1014 56 92 169 3.15 {ENTER}
sleep, 2000 ;Wait 2s for zone load
Send {ENTER}
sleep, 200
Send, /g Initializing Death Maze Setup Script... {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1014 62 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1014 68 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1014 74 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1014 80 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1014 86 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200

;Boss Event 1 x3
Send {ENTER}
sleep, 200
Send, .go xyz 1024 65 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1024 83 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1050 73 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888885 {ENTER}
sleep, 200

;Diablo x2
Send {ENTER}
sleep, 200
Send, .go xyz 1091 61 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1091 85 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER}
sleep, 200

;Boss Event Mob x4
Send {ENTER}
sleep, 200
Send, .go xyz 1149 59 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1149 68 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1149 77 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1149 86 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189999 {ENTER}
sleep, 200

;Shark King x2
Send {ENTER}
sleep, 200
Send, .go xyz 1200 66 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1200 79 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER}
sleep, 200

;Boss Event 2 x3
Send {ENTER}
sleep, 200
Send, .go xyz 1276.7 49.5 92 169 1.56 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888887 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1240 23 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888887 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1180 10 92 169 1.5 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 888887 {ENTER}
sleep, 200

;Terokk x1
Send {ENTER}
sleep, 200
Send, .go xyz 1140 28 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200

;Earth Revenant x3
Send {ENTER}
sleep, 200
Send, .go xyz 1115 38 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1115 8 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1090 23 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200

;Therazane x2
Send {ENTER}
sleep, 200
Send, .go xyz 1060 38 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253152 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1060 8 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253152 {ENTER}
sleep, 200

;Terokk x2
Send {ENTER}
sleep, 200
Send, .go xyz 980 30 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 980 10 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200003 {ENTER}
sleep, 200

;!UN-USED GM SLOT! (GM Boss)
;Send {ENTER}
;sleep, 200
;Send, .go xyz 1020 20 92 169 0 {ENTER}
;sleep, 200
;Send {ENTER}
;sleep, 200
;Send, .npc add [ INSERT BOSS ID ] {ENTER}
;sleep, 200
;Send {ENTER}
;sleep, 200
;Send, /g GM Boss >> [ INSERT BOSS NAME ] << spawned in! {ENTER}
;sleep, 200

;Vengerful (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 930 20 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 1000361 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Vengerful ] << spawned in! {ENTER}
sleep, 200

;Smolderon x1
Send {ENTER}
sleep, 200
Send, .go xyz 1350 23 92 169 3.15 {ENTER}
sleep, 500 ;allow short time for load
Send {ENTER}
sleep, 200
Send, .npc add 189930 {ENTER}
sleep, 200

;Shark x2
Send {ENTER}
sleep, 200
Send, .go xyz 1400 35 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1400 10 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER}
sleep, 200

;Diablo x3
Send {ENTER}
sleep, 200
Send, .go xyz 1445 36 92 169 4.7 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1480 74 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1375 87 92 169 6.25 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200007 {ENTER}
sleep, 200

;Shark King x1
Send {ENTER}
sleep, 200
Send, .go xyz 1375 60 92 169 6.25 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200008 {ENTER}
sleep, 200

;Ahune x1
Send {ENTER}
sleep, 200
Send, .go 1340 122 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253151 {ENTER}
sleep, 200

;Tempest Revenant x5
Send {ENTER}
sleep, 200
Send, .go xyz 1320 98 92 169 4.75 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1235 110.5 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1275 180 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1340 160 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1340 185 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200

;Fire Revenant x1
Send {ENTER}
sleep, 200
Send, .go xyz 1385 120 92 169 1.5 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 202004 {ENTER}
sleep, 200

;Chron (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 1480 123 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 1000370 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Chron ] << spawned in! {ENTER}
sleep, 200

;Earth Revenant x2
Send {ENTER}
sleep, 200
Send, .go xyz 1155 178 92 169 0 {ENTER}
sleep, 500 ;allow short wait for load
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1155 163 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200

;Therazane x2
Send {ENTER}
sleep, 200
Send, .go xyz 1090 170 92 169 6.25 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253152 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1050 170 92 169 6.25 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253152 {ENTER}
sleep, 200

;Earth Revenant x2
Send {ENTER}
sleep, 200
Send, .go xyz 960 180 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 960 160 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121292 {ENTER}
sleep, 200

;Smolderon x1
Send {ENTER}
sleep, 200
Send, .go xyz 930 220 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189930 {ENTER}
sleep, 200

;Fire Revenant x2
Send {ENTER}
sleep, 200
Send, .go xyz 970 255 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 202004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 970 290 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 202004 {ENTER}
sleep, 200

;Prince Thunderaan x2
Send {ENTER}
sleep, 200
Send, .go xyz 1065 255 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200005 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1065 290 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200005 {ENTER}
sleep, 200

;Water Revenant x3
Send {ENTER}
sleep, 200
Send, .go xyz 1160 270 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1260 275 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1330 275 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200

;Pallemaster (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 1440 275 92 169 3.15 {ENTER}
sleep, 500 ;allow short time for load
Send {ENTER}
sleep, 200
Send, .npc add 1000366 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Pallemaster ] << spawned in! {ENTER}
sleep, 200

;Dot (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 1170 220 92 169 0 {ENTER}
sleep, 500 ;allow short wait period
Send {ENTER}
sleep, 200
Send, .npc add 1000356 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Dot ] << spawned in! {ENTER}
sleep, 200

;Notus
Send {ENTER}
sleep, 200
Send, .go xyz 1240 220 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 566003 {ENTER}
sleep, 200

;Boreas
Send {ENTER}
sleep, 200
Send, .go xyz 1300 220 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 566002 {ENTER}
sleep, 200

;Bacchus
Send {ENTER}
sleep, 200
Send, .go xyz 1360 220 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 556000 {ENTER}
sleep, 200

;Astaroth
Send {ENTER}
sleep, 200
Send, .go xyz 1420 220 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 556001 {ENTER}
sleep, 200

;Archimonde
Send {ENTER}
sleep, 200
Send, .go xyz 1480 220 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 55570 {ENTER}
sleep, 200

;Mirel (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 1453 172.5 92 169 1.5 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 1000363 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Mirel ] << spawned in! {ENTER}
sleep, 200

;Smolderon x1
Send {ENTER}
sleep, 200
Send, .go xyz 1415 345 92 169 0 {ENTER}
sleep, 500 ;allow short wait period
Send {ENTER}
sleep, 200
Send, .npc add 189930 {ENTER}
sleep, 200

;Ahune x1
Send {ENTER}
sleep, 200
Send, .go xyz 1415 320 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253151 {ENTER}
sleep, 200

;Fire Revenant x4
Send {ENTER}
sleep, 200
Send, .go xyz 1360 345 92 169 4.7 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 202004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1315 375 92 169 4.7 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 202004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1315 420 92 169 4.7 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 202004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1275 420 92 169 4.7 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 202004 {ENTER}
sleep, 200

;Smolderon x1
Send {ENTER}
sleep, 200
Send, .go xyz 1210 395 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 189930 {ENTER}
sleep, 200

;Water Revenant x4
Send {ENTER}
sleep, 200
Send, .go xyz 1145 410 92 169 4.7 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1145 375 92 169 1.5 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1090 405 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1090 375 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 121291 {ENTER}
sleep, 200

;Ahune x2
Send {ENTER}
sleep, 200
Send, .go xyz 1040 405 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253151 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 1040 375 92 169 0 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 253151 {ENTER}
sleep, 200

;Luminia (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 1060 330 92 169 0 {ENTER}
sleep, 500 ;allow short wait for load
Send {ENTER}
sleep, 200
Send, .npc add 1000357 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Luminia ] << spawned in! {ENTER}
sleep, 200

;Sarah (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 920 330 92 169 0 {ENTER}
sleep, 500 ;allow short wait for load
Send {ENTER}
sleep, 200
Send, .npc add 1000362 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Sarah ] << spawned in! {ENTER}
sleep, 200

;Carn (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 990 392.5 92 169 3.15 {ENTER}
sleep, 500 ;allow short wait for load
Send {ENTER}
sleep, 200
Send, .npc add 1000358 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Carn ] << spawned in! {ENTER}
sleep, 200

;Fypen (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 920 490 92 169 4.7 {ENTER}
sleep, 500 ;allow short wait for load
Send {ENTER}
sleep, 200
Send, .npc add 1000364 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Fypen ] << spawned in! {ENTER}
sleep, 200

;Tempest Revenant x2 & Prince Thunderaan x2
Send {ENTER}
sleep, 200
Send, .go xyz 990 440 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 990 450 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200004 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 990 465 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200005 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .go xyz 990 485 92 169 3.15 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, .npc add 200005 {ENTER}
sleep, 200

;Jawless (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 1095 465 92 169 3.15 {ENTER}
sleep, 500 ;allow short wait for load
Send {ENTER}
sleep, 200
Send, .npc add 1000360 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Jawless ] << spawned in! {ENTER}
sleep, 200

;Parasinus (GM Boss)
Send {ENTER}
sleep, 200
Send, .go xyz 1200 465 92 169 3.15 {ENTER}
sleep, 500 ;allow short wait period
Send {ENTER}
sleep, 200
Send, .npc add 35329 {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g GM Boss >> [ Parasinus ] << spawned in! {ENTER}
sleep, 200
Send {ENTER}
sleep, 200
Send, /g Death Maze Setup finished... {ENTER}
sleep, 250




$F2::pause ;(un)pause
return
$F3::reload ;reload from the beginning